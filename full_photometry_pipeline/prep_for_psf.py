#!/usr/bin/env python


import numpy as np
import re
import sys
import pexpect ## Use this to control daophot
import shutil
from astropy.io import fits
import os
import subprocess
import string
import glob

def prep_for_psf(target, cat_name, nstars='9999', faintmag='24.0', single_psf=True, multi_psf = False, auto_psfs=False, chan='3p6um'):
  
	if single_psf==True:

		fitsfile = target + '_e1_3p6um_dn.fits'
		
		#find RRLy pixel position, save to text file 'rrly'

		with open(cat_name, 'r') as searchfile:
			for line in searchfile:
				if target in line:
					data = line.split()
					if data[0]==target:
						rrra = data[1]
						rrdec = data[2]

		pixelpos = subprocess.check_output('sky2xy -j ' + fitsfile +' '+ rrra + ' ' + rrdec, shell=True)
		pixx = pixelpos.split()[4]
		pixy = pixelpos.split()[5]

		rrfile = open('./rrly', 'w')
		rrfile.write(pixx + ' ' + pixy)
		rrfile.close()

		#pick psf star candidates from the .ap file, save to psflist

		daophot = pexpect.spawn("daophot")
		daophot.expect("Command:")
		daophot.sendline("opt")
		daophot.expect("File with parameters")
		daophot.sendline("")
		daophot.expect("OPT>")
		daophot.sendline("PS=4")
		daophot.expect("OPT>")
		daophot.sendline("")
		daophot.expect("Command:")
		daophot.sendline("pick")
		daophot.expect("Input file name:")
		daophot.sendline(target+'_e1_3p6um_dn.ap')
		daophot.expect("Desired number of stars, faintest magnitude:")
		daophot.sendline(nstars + ', ' + faintmag)
		daophot.expect("Output file name")
		daophot.sendline(target + '_e1_3p6um_dn_pick.lst')
		daophot.expect("Command:")
		daophot.sendline("exit")


		psfstarfile = open('./'+target+'_e1_3p6um_dn.lst','w')
		psfstarfile.close()

		psfstarpickfile = open('./'+target+'_e1_3p6um_dn_pick.lst','r')
		postpickfile = open('./'+target+'_e1_3p6um_dn_postpick.lst','w')

		print "Cleaning psflist"

		counter = 0
		
		expt_image = '../masters/'+target+'/'+target+'_3p6um_expt.fits'
		expt_fits = fits.open(expt_image, mode="readonly")
		expt_data = expt_fits[0].data
		expt_max = np.max(expt_data)
		
		for starline in psfstarpickfile:
			if counter <= 2: postpickfile.write(starline)

			if counter > 2:
				id, xpos, ypos, mag, magerr = string.split(starline)
				epoch1_ra, epoch1_dec , dfas, dsafs, fasdf = string.split(subprocess.check_output('xy2sky -j ' + target +'_e1_3p6um_dn.fits '+ xpos + ' ' + ypos, shell=True))
				expt_string = subprocess.check_output('sky2xy -j ../masters/'+target+'/' + target+ '_3p6um_expt.fits ' + epoch1_ra + ' ' + epoch1_dec, shell=True)
				expt_x = float(expt_string.split()[4])
				expt_y = float(expt_string.split()[5])
				try:expt_value = expt_data[int(np.floor(expt_x)), int(np.floor(expt_y))]
				except: expt_value = -1
				magerr=float(magerr)
				if (expt_value > expt_max*.7):
					if ((magerr < .0230) & (magerr > 0.0080)):
							postpickfile.write(starline)
			counter = counter+1

		postpickfile.close()

		os.system('tail -n +4 ' + target + '_e1_3p6um_dn_postpick.lst | gawk \'{print $2,$3}\' > psflist')

		print 'Made rrly and psflist, _pick.lst, _postpick.lst and .lst'

		if os.path.isfile('psfstars'):
			psf_stars_chosen=True
			print "File \'psfstars\' found, rebuilding .lst file"
			if os.path.isfile(target + '_e1_3p6um_dn.lst'): shutil.copy(target + '_e1_3p6um_dn.lst', target + '_e1_3p6um_dn.lst_save')

			psfstarlistfile = open('./'+target+'_e1_3p6um_dn.lst','w')
			psfstarfile = open('psfstars', 'r')

			for psfstarline in psfstarfile:
				xpos, ypos = string.split(psfstarline)
				with open(target + '_e1_3p6um_dn_pick.lst') as searchfile:
					for line in searchfile:
						if (xpos + '  ' + ypos) in line:
							psfstarlistfile.write(line)


			psfstarlistfile.close()
			psfstarfile.close()
			searchfile.close()

			#make psf
			image = target + '_e1_3p6um_dn'
			daophot = pexpect.spawn("daophot")
			daophot.logfile = sys.stdout
			daophot.expect("Command:")
			daophot.sendline("at " + image + '.fits')
			daophot.expect("Command:")
			daophot.sendline("psf")
			daophot.expect("File with aperture results")
			daophot.sendline("")
			daophot.expect("File with PSF stars")
			daophot.sendline("")
			daophot.expect("File for the PSF")
			daophot.sendline("")
			daophot.expect("Command:")
			daophot.sendline("exit")
			print "PSF made"

			#make allstar for e1
			allstar = pexpect.spawn("allstar")
			allstar.logfile = sys.stdout
			allstar.expect("OPT>")
			allstar.sendline("")
			allstar.expect("Input image name")
			allstar.sendline(image + ".fits")
			allstar.expect("File with the PSF")
			allstar.sendline(image + ".psf")
			allstar.expect("Input file")
			allstar.sendline(image +".ap")
			allstar.expect("File for results")
			allstar.sendline(image +".als")
			allstar.expect("Name for subtracted image")
			allstar.sendline(image +"s.fits")
			allstar.expect("stars")
			allstar.expect("Good bye")
			allstar.close()
			print "ALLSTAR complete"
		else:psf_stars_chosen=False

	if multi_psf == True:
		for i in np.arange(12)+1:
			epoch = 'e'+str(i)
			fitsfile = target + '_'+epoch+'_3p6um_dn.fits'
			
			#find RRLy pixel position, save to text file 'rrly'

			with open(cat_name, 'r') as searchfile:
				for line in searchfile:
					if target in line:
						data = line.split()
						if data[0]==target:
							rrra = data[1]
							rrdec = data[2]

			pixelpos = subprocess.check_output('sky2xy -j ' + fitsfile +' '+ rrra + ' ' + rrdec, shell=True)
			pixx = pixelpos.split()[4]
			pixy = pixelpos.split()[5]

			rrfile = open('./rrly_'+epoch, 'w')
			rrfile.write(pixx + ' ' + pixy)
			rrfile.close()

			#pick psf star candidates from the .ap file, save to psflist

			daophot = pexpect.spawn("daophot")
			daophot.expect("Command:")
			daophot.sendline("opt")
			daophot.expect("File with parameters")
			daophot.sendline("")
			daophot.expect("OPT>")
			daophot.sendline("PS=4")
			daophot.expect("OPT>")
			daophot.sendline("")
			daophot.expect("Command:")
			daophot.sendline("pick")
			daophot.expect("Input file name:")
			daophot.sendline(target+'_'+epoch+'_3p6um_dn.ap')
			daophot.expect("Desired number of stars, faintest magnitude:")
			daophot.sendline(nstars + ', ' + faintmag)
			daophot.expect("Output file name")
			daophot.sendline(target + '_'+epoch+'_3p6um_dn_pick.lst')
			daophot.expect("Command:")
			daophot.sendline("exit")


			psfstarfile = open('./'+target+'_'+epoch+'_3p6um_dn.lst','w')
			psfstarfile.close()

			psfstarpickfile = open('./'+target+'_'+epoch+'_3p6um_dn_pick.lst','r')
			postpickfile = open('./'+target+'_'+epoch+'_3p6um_dn_postpick.lst','w')

			print "Cleaning psflist for "+epoch

			counter = 0
			
			expt_image = '../masters/'+target+'/'+target+'_3p6um_expt.fits'
			expt_fits = fits.open(expt_image, mode="readonly")
			expt_data = expt_fits[0].data
			expt_max = np.max(expt_data)
			
			for starline in psfstarpickfile:
				if counter <= 2: postpickfile.write(starline)

				if counter > 2:
					id, xpos, ypos, mag, magerr = string.split(starline)
					epoch1_ra, epoch1_dec , dfas, dsafs, fasdf = string.split(subprocess.check_output('xy2sky -j ' + target +'_'+epoch+'_3p6um_dn.fits '+ xpos + ' ' + ypos, shell=True))
					expt_string = subprocess.check_output('sky2xy -j ../masters/'+target+'/' + target+ '_3p6um_expt.fits ' + epoch1_ra + ' ' + epoch1_dec, shell=True)
					expt_x = float(expt_string.split()[4])
					expt_y = float(expt_string.split()[5])
					expt_value = expt_data[int(np.floor(expt_x)), int(np.floor(expt_y))]
					magerr=float(magerr)
					if (expt_value > expt_max*.8):
						if ((magerr < .0200) & (magerr > 0.010)):
								postpickfile.write(starline)
				counter = counter+1

			postpickfile.close()

			os.system('tail -n +4 ' + target + '_'+epoch+'_3p6um_dn_postpick.lst | gawk \'{print $2,$3}\' > psflist_'+epoch)

			print 'Made rrly and psflist, _pick.lst, _postpick.lst and .lst for '+epoch

			if os.path.isfile('psfstars_'+epoch):
				print "File \'psfstars\' for "+epoch+" found, rebuilding .lst file"
				if os.path.isfile(target + '_'+epoch+'_3p6um_dn.lst'): shutil.copy(target + '_'+epoch+'_3p6um_dn.lst', target + '_'+epoch+'_3p6um_dn.lst_save')

				psfstarlistfile = open('./'+target+'_'+epoch+'_3p6um_dn.lst','w')
				psfstarfile = open('psfstars_'+epoch, 'r')

				for psfstarline in psfstarfile:
					xpos, ypos = string.split(psfstarline)
					with open(target + '_'+epoch+'_3p6um_dn_pick.lst') as searchfile:
						for line in searchfile:
							if (xpos + '  ' + ypos) in line:
								psfstarlistfile.write(line)


				psfstarlistfile.close()
				psfstarfile.close()
				searchfile.close()

				#make psf
				image = target + '_'+epoch+'_3p6um_dn'
				daophot = pexpect.spawn("daophot")
				daophot.logfile = sys.stdout
				daophot.expect("Command:")
				daophot.sendline("at " + image + '.fits')
				daophot.expect("Command:")
				daophot.sendline("psf")
				daophot.expect("File with aperture results")
				daophot.sendline("")
				daophot.expect("File with PSF stars")
				daophot.sendline("")
				daophot.expect("File for the PSF")
				daophot.sendline("")
				daophot.expect("Command:")
				daophot.sendline("exit")
				print epoch+" PSF made"

				#make allstar for e1
				allstar = pexpect.spawn("allstar")
				allstar.logfile = sys.stdout
				allstar.expect("OPT>")
				allstar.sendline("")
				allstar.expect("Input image name")
				allstar.sendline(image + ".fits")
				allstar.expect("File with the PSF")
				allstar.sendline(image + ".psf")
				allstar.expect("Input file")
				allstar.sendline(image +".ap")
				allstar.expect("File for results")
				allstar.sendline(image +".als")
				allstar.expect("Name for subtracted image")
				allstar.sendline(image +"s.fits")
				allstar.expect("stars")
				allstar.expect("Good bye")
				allstar.close()
				print epoch+" ALLSTAR complete"

	#if (os.path.isfile('psfstars_e'+str(i))==True for i in np.arange(12)+1): psf_stars_chosen=True
	#else:psf_stars_chosen=False





	if auto_psfs==True:

		file_list = glob.glob('*_e*.ap')
		epoch1_file = target+'_e1_3p6um_dn.ap'
		file_list.remove(epoch1_file)
		mch_file = target + '_3p6um.mch'
		if (os.path.isfile(mch_file)): os.remove(mch_file)
		## run daomatch
		print "Running daomatch"

		daomatch = pexpect.spawn("daomatch")
		daomatch.expect('Master input file:')
		daomatch.sendline(epoch1_file)
		daomatch.expect('Output file name')
		daomatch.sendline(mch_file)
		print "made the mch file"
		## Loop over the file_list
		for ap_file in file_list:
			index = daomatch.expect(['Next input file', 'Write this transformation'])
			if index == 0:
				daomatch.sendline(ap_file)
			elif index == 1:
				daomatch.sendline('y')
				daomatch.expect('Next input file')
				daomatch.sendline(ap_file)
			print "finished " + ap_file
		## Exiting line
		index = daomatch.expect(['Next input file', 'Write this transformation'])
		if index == 0:
			daomatch.sendline("")
		elif index == 1:
			daomatch.sendline('y')
			daomatch.expect('Next input file')
			daomatch.sendline("")
		daomatch.close(force=True)

		## running daomaster
		print "running daomaster"
		num_frames = len(file_list) + 1
		print num_frames

		daomaster = pexpect.spawn("daomaster")
		daomaster.logfile=open('./daomasterlog', 'w')
		daomaster.expect("File with list of input files")
		daomaster.sendline(mch_file)
		print mch_file
		daomaster.expect("Minimum number, minimum fraction, enough frames")
		daomaster.sendline(str(num_frames) + ", 1, " + str(num_frames))
		print str(num_frames) + ", 1, " + str(num_frames)
		daomaster.expect("Maximum sigma")
		daomaster.sendline("99")
		## desired degrees of freedom:
		daomaster.expect("Your choice")
		daomaster.sendline("6")
		daomaster.expect("Critical match-up radius")
		daomaster.sendline("120")
		for i in np.arange(num_frames):
			daomaster.sendline("")

		for radius in range (120,-1, -1):
			daomaster.expect("New match-up radius")
			daomaster.sendline(str(radius))

		## Only need the transformations here
		print "making output files"
		daomaster.expect("Assign new star IDs")
		daomaster.sendline("n")
		daomaster.expect("A file with mean magnitudes and scatter")
		daomaster.sendline("n")
		daomaster.expect("A file with corrected magnitudes and errors")
		daomaster.sendline("n")
		daomaster.expect("A file with raw magnitudes and errors")
		daomaster.sendline("n")
		daomaster.expect("A file with the new transformations")
		daomaster.sendline("n")
		daomaster.expect("A file with the transfer table")
		daomaster.sendline("n")
		daomaster.expect("Individual .COO files?")
		daomaster.sendline("n")
		#daomaster.expect("This file already exists:")
		#daomaster.sendline("e1-psf.coo")
		#for i in np.arange(len(file_list)):
		#	ep = string.split(file_list[i],'_')[1]
		#	daomaster.expect("This file already exists:")
		#	daomaster.sendline(ep+'-psf.coo')
		daomaster.expect("Simply transfer star IDs?")
		daomaster.sendline("y")
		daomaster.sendline("y")

		daomaster.close(force=True)

		psfstarlistfile = open('./'+target+'_e1_3p6um_dn_postpick.lst','w')
		psfstarpickfile = open('./'+target+'_e1_3p6um_dn_pick.lst','r')
		epoch1mtrfile = open('./'+target+'_e1_3p6um_dn.mtr','r')

		mtrfiles = glob.glob('./'+target+'_e*_3p6um_dn.mtr')
		openmtrfiles = [open(mtrfile, 'r') for mtrfile in mtrfiles]
		postpickfiles = [open(re.sub('.mtr','_postpick.lst', mtrfile), 'w') for mtrfile in mtrfiles]

		matched_ids, matched_x, matched_y, matched_mag = np.loadtxt('./'+target+'_e1_3p6um_dn.mtr', skiprows=3, usecols=(0,1,2,3), unpack=True)
		picked_ids, picked_x, picked_y, picked_mag, picked_magerr = np.loadtxt('./'+target+'_e1_3p6um_dn_pick.lst', skiprows=3, usecols=(0,1,2,3,4), unpack=True)
		
		counter = 0
		n_picked_matched=0
		
		expt_image = '../masters/'+target+'/'+target+'_3p6um_expt.fits'
		expt_fits = fits.open(expt_image, mode="readonly")
		expt_data = expt_fits[0].data
		expt_max = np.max(expt_data)
		
		for starline in epoch1mtrfile:
			if counter > 2:
				id, xpos, ypos, mag, tmp,tmp,tmp,tmp,tmp = string.split(starline)
				if float(id) in picked_ids:
					magerr = picked_magerr[picked_ids==float(id)]
					epoch1_ra, epoch1_dec , dfas, dsafs, fasdf = string.split(subprocess.check_output('xy2sky -j ' + target +'_e1_3p6um_dn.fits '+ xpos + ' ' + ypos, shell=True))
					expt_string = subprocess.check_output('sky2xy -j ../masters/'+target+'/' + target+ '_3p6um_expt.fits ' + epoch1_ra + ' ' + epoch1_dec, shell=True)
					expt_x = float(expt_string.split()[4])
					expt_y = float(expt_string.split()[5])
					expt_value = expt_data[int(np.floor(expt_x)), int(np.floor(expt_y))]
					magerr=float(magerr)
					if (expt_value > expt_max*.8):
						if ((magerr < .0200) & (magerr > 0.010)):
							for i in np.arange(12):
								openmtrfiles[i].seek(145+80*(counter-3))
								postpickfiles[i].write(openmtrfiles[i].readline())

							#psfstarlistfile.write(starline)
							n_picked_matched+=1
					#check pixel value on master, compare with max 
					#reject if < 90%?
					#if thing also good then add to .lst
			counter = counter+1

		print n_picked_matched, ' stars found in all frames meeting psf star criteria'

		psfstarlistfile.close()
		psfstarpickfile.close()
		epoch1mtrfile.close()
		for openmtrfile in openmtrfiles: openmtrfile.close()
		for postpickfile in postpickfiles: postpickfile.close()

		os.system('rm *postpick.psf *postpick.nei *cleaned.psf *cleaned.nei')

		for i in np.arange(12)+1:
			epoch = str(i)
			daophot = pexpect.spawn("daophot")
			daophot.logfile = open('./psflog_e'+epoch, 'w')
			daophot.expect("Command:")
			daophot.sendline("at " + target + '_e' + epoch + '_3p6um_dn.fits')
			daophot.expect("Command:")
			daophot.sendline("psf")
			daophot.expect("File with aperture results")
			daophot.sendline(target+'_e'+epoch+'_3p6um_dn.ap')
			daophot.expect("File with PSF stars")
			daophot.sendline(target+'_e'+epoch+'_3p6um_dn_postpick.lst')
			daophot.expect("File for the PSF")
			daophot.sendline(target+'_e'+epoch+'_3p6um_dn_postpick.psf')
			daophot.expect("Command:")
			daophot.sendline("exit")

		print 'inital psf run complete'

		for i in np.arange(12)+1:
			epoch = str(i)
			psflog = open('./psflog_e'+epoch, 'r')
			oldlst = open(target+'_e'+epoch+'_3p6um_dn_postpick.lst', 'r')
			newlst = open(target+'_e'+epoch+'_3p6um_dn_cleaned.lst', 'w')
			thisline = psflog.readline()
			checker = 0
			while ' Profile errors:' not in thisline:
				thisline = psflog.readline()
			psflog.readline()
			psfoutput = []
			while thisline != '\r\n':
				thisline=psflog.readline()
				splits = thisline.split()
				psfoutput += splits
			psflog.close()
			while '?' in psfoutput: psfoutput.remove('?')
			while '*' in psfoutput: psfoutput.remove('*')
			psfdata = np.reshape(psfoutput,(-1,2))
			for line in oldlst:
				id = line.split()[0]
				try: 
					chi = psfdata[psfdata[...,0]==id][0][1]
					if float(chi) < 0.1:
						newlst.write(line)
				except: continue


			newlst.close()
			oldlst.close()

			os.system('wc -l RR4_e'+epoch+'_3p6um_dn_cleaned.lst')


		for i in np.arange(12)+1:
			epoch = str(i)
			daophot = pexpect.spawn("daophot")
			daophot.logfile = open('./psflog_e'+epoch+'_c', 'w')
			daophot.expect("Command:")
			daophot.sendline("at " + target + '_e' + epoch + '_3p6um_dn.fits')
			daophot.expect("Command:")
			daophot.sendline("psf")
			daophot.expect("File with aperture results")
			daophot.sendline(target+'_e'+epoch+'_3p6um_dn.ap')
			daophot.expect("File with PSF stars")
			daophot.sendline(target+'_e'+epoch+'_3p6um_dn_cleaned.lst')
			daophot.expect("File for the PSF")
			daophot.sendline(target+'_e'+epoch+'_3p6um_dn_cleaned.psf')
			daophot.expect("Command:")
			daophot.sendline("exit")

		print 'made cleaned psfs'

		#make allstar for e1
		allstar = pexpect.spawn("allstar")
		allstar.logfile = sys.stdout
		allstar.expect("OPT>")
		allstar.sendline("")
		allstar.expect("Input image name")
		allstar.sendline(target + "_e1_3p6um_dn.fits")
		allstar.expect("File with the PSF")
		allstar.sendline(target + "_e1_3p6um_dn_cleaned.psf")
		allstar.expect("Input file")
		allstar.sendline(target + "_e1_3p6um_dn.ap")
		allstar.expect("File for results")
		allstar.sendline(target + "_e1_3p6um_dn.als")
		allstar.expect("Name for subtracted image")
		allstar.sendline(target + "_e1_3p6um_dns.fits")
		allstar.expect("stars")
		allstar.expect("Good bye")
		allstar.close()
		print "ALLSTAR complete"


		psf_stars_chosen = True


	return psf_stars_chosen


